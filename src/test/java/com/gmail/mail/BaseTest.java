package com.gmail.mail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.gmail.mail.core.ConciseAPI;

/**
 * Created by aru on 2017-09-22.
 */
public class BaseTest extends ConciseAPI{

    static WebDriver driver;

    @BeforeClass
    public static void initializeDriver() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    public WebDriver getWebDriver() {
        return driver;
    }
}
