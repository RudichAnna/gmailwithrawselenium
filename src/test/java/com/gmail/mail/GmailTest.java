package com.gmail.mail;

import com.gmail.mail.pages.GmailPage;
import com.gmail.mail.pages.MailsPage;
import com.gmail.mail.pages.MenuPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by aru on 2017-09-21.
 */
public class GmailTest extends BaseTest {

    MenuPage menuPage = new MenuPage(driver);
    MailsPage mailsPage = new MailsPage(driver);
    GmailPage gmailPage = new GmailPage(driver);

    String subject = "TestSubject" + System.currentTimeMillis();


    @Test
    public void loginSendRecieveAndSearch() {

        gmailPage.visit();

        gmailPage.logInAs("janeairtest2017@gmail.com", "Gdansk2017");

        mailsPage.send("janeairtest2017@gmail.com", subject);
        menuPage.refresh();
        mailsPage.assertMail(0, subject);

        menuPage.goToSent();
        mailsPage.assertMail(0, subject);

        menuPage.goToInbox();
        mailsPage.searchMailbox(subject);

        mailsPage.assertMails("boo");
    }
}



