package com.gmail.mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * Created by aru on 2017-09-22.
 */
public class GmailPage extends BasePage {

    public GmailPage(WebDriver driver) {
        super(driver);
    }

    public void visit(){
        open("http://gmail.com/");
    }

    public void logInAs(String mail, String password) {
       setValue("#identifierId", mail + Keys.ENTER);
       setValue("input[type='password']", password + Keys.ENTER);
    }
}
