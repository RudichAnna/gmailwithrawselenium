package com.gmail.mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by aru on 2017-09-22.
 */
public class MenuPage extends BasePage {

    public MenuPage(WebDriver driver) {
        super(driver);
    }

    public void refresh() {
        $(By.cssSelector(".asf")).click();
    }

    public void goToInbox() {
        $(By.cssSelector("a[title*='Inbox']")).click();
    }

    public void goToSent() {
        $(By.cssSelector("a[title*='Sent Mail']")).click();
    }
}
