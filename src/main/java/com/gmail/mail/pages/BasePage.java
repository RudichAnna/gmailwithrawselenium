package com.gmail.mail.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.gmail.mail.core.ConciseAPI;

/**
 * Created by aru on 2017-09-22.
 */
public class BasePage extends ConciseAPI {

    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getWebDriver() {
        return driver;
    }
}
