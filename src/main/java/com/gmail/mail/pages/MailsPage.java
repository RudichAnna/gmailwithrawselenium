package com.gmail.mail.pages;

import com.gmail.mail.core.CustomConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

/**
 * Created by aru on 2017-09-22.
 */
public class MailsPage extends BasePage {

    @FindBy(xpath = "//*[text()='COMPOSE']")
    WebElement composeButton;

    @FindBy(name = "to")
    WebElement to;

    @FindBy(css = "[role='main'] .zA")
    List<WebElement> mailsList;


    public MailsPage(WebDriver driver) {
        super(driver);
    }

    public void assertMail(int index, String headerText){
        assertThat(CustomConditions.listNthElementHasText(mailsList, index, headerText));
    }

    public void assertMails(String... headerTexts) {
        for (String headerText : headerTexts) {
            assertThat(CustomConditions.textsOf(mailsList, headerText), 4);
        }
    }

    public void searchMailbox(String searchQuery) {
        $(By.name("q")).clear();
        $(By.name("q")).sendKeys(searchQuery + Keys.ENTER);
    }

    public void send(String eMail, String subject) {
        byText("COMPOSE").click();

        setValue(By.name("to"), eMail);
        setValue(By.name("subjectbox"), subject);

        $(By.xpath("//*[text()='Send']")).click();
    }
}
