package com.gmail.mail.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Quotes;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by aru on 2017-09-22.
 */
public abstract class ConciseAPI {

    public abstract WebDriver getWebDriver();

    public void open(String url) {
        getWebDriver().get(url);
    }

    public WebElement $(By locator) {
        assertThat(ExpectedConditions.visibilityOfElementLocated(locator));
        return getWebDriver().findElement(locator);
    }

    public WebElement byText(String elementText){
        return $(By.xpath(".//*/text()[normalize-space(.) = " + Quotes.escape(elementText) + "]/parent::*"));
    }

    public void setValue(By elementLocator, String value) {
        $(elementLocator).clear();
        $(elementLocator).sendKeys(value);
    }

    public void setValue(String cssSelector, String value) {
        setValue(By.cssSelector(cssSelector), value);
    }

    public void  assertThat(ExpectedCondition condition, long timeout) {
        new WebDriverWait(getWebDriver(), timeout).until(condition);
    }

    public void assertThat(ExpectedCondition condition) {
        assertThat(condition, Configuration.timeout);
    }

//    потом - когда assertThat(visibilityOfElementLocated(...)) будет возвращать вебэлемент - снова упростишь метод $
}
