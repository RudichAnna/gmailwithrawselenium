package com.gmail.mail.core;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aru on 2017-09-28.
 */
public class CustomConditions {

    public static ExpectedCondition<Boolean> listNthElementHasText(final List<WebElement> elements, final int index, final String expectedText) {

        return new ExpectedCondition<Boolean>() {
            private String elementText;


            public Boolean apply(WebDriver driver) {
                try {
                    elementText = elements.get(index).getText();
                    return elementText.contains(expectedText);
                } catch (StaleElementReferenceException e) {
                    return false;
                }
            }

            public String toString() {
                return String.format("\ntext in %s\n" +
                                " element of the elements list  has to be: " +
                                "%s\n while actual text is: %s\n",
                        index, expectedText, elementText);
            }
        };
    }

    public static ExpectedCondition<Boolean> textsOf(final List<WebElement> elements, final String... expectedTexts) {

        final List<String> actualTexts = new ArrayList<String>();

        for (int i = 0; i < elements.size(); i++)
            try {
                actualTexts.add(elements.get(i).getText());
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Warning: IndexOutOfBoundsException");
            }

        return new ExpectedCondition<Boolean>() {

            public Boolean apply(WebDriver driver) {

                if (actualTexts.size() != expectedTexts.length) {
                    return false;
                }

                for (int i = 0; i < expectedTexts.length; i++) {
                    if (!actualTexts.get(i).contains(expectedTexts[i])) {
                        return false;
                    }
                }
                return true;

            }

            /* мне не нравится такой месседж, но у меня нет пока идей как усделать лучше
             expected [Ljava.lang.String;@4229bb3f
            element text has to be: [Ljava.lang.String;@4229bb3f
             while actual text was: [me
            TestSubject1508917498912
              09:45, me
            TestSubject1508917164087
              09:39, me
            TestSubject1508855678158
              24 Oct]
             */
            public String toString() {
                return String.format("\n for %s\n expected %s\n" +
                        "element text has to be: %s\n" +
                        " while actual text was: %s\n", elements, expectedTexts, expectedTexts, actualTexts);
            }
        };
    }
}
